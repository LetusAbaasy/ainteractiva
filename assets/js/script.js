/**
	Creado para AInteractiva.
	@Autor: Sergio Daniel Martinez Porras
	@Fecha Creación: 22/11/2018-19:40
	@Fecha Modificación: 28/11/2018-16:46
**/

//Funcion ready para cuando este cargado el documento
$(document).ready(function(){
	//Inicializacion de aplicación
	init();
	//Variables si existen
	var player;
	//Funciones en ready
		//Funcion para boton de ver resultados de aranda
	$("#verResultadosRepAranda").click(function(){
		//url del valor del option select
		$url = $("#opcionesResultadosRepAranda").val();
		//Abrir nueva ventana de explorador
		console.log("Abrir URL:"+$url);
	    //window.open($url);
	    $("#pageOpen").attr("src", $url);
	    //Retornar false
	    return false;
  	});

		//Funcion para boton de ver resultados de cmd
  	$("#verResultadosRepCms").click(function(){
		//url del valor del option select
		$url = $("#opcionesResultadosRepCms").val();
		//Abrir nueva ventana de explorador
		console.log("Abrir URL:"+$url);
	    //window.open($url);
	    $("#pageOpen").attr("src", $url);
	    //Retornar false
	    return false;
  	});
  	
  		//Funcion para boton de ver resultados de cmd
  	$("#verResultadosRepEncuesta").click(function(){
		//url del valor del option select
		$url = $("#opcionesResultadosRepEncuesta").val();
		//Abrir nueva ventana de explorador
		console.log("Abrir URL:"+$url);
	    //window.open($url);
	    $("#pageOpen").attr("src", $url);
	    //Retornar false
	    return false;
  	});

  	$("#video").hover(function(){
	   	$("#AInteractiva").hide();
		player = new YT.Player('videoAinteractiva', {
			height: '90',
			width: '120',
			videoId: 'akanXAjmNXw',
			playerVars: { 'autoplay': 1, 'controls': 0,'autohide': 1, 'wmode':'opaque' },
			events: {
				'onStateChange': cambioEstadoVideo
			}
    	});
	});
});

function cambioEstadoVideo(event,element) {
    if (event.data == YT.PlayerState.ENDED) {
        $("#AInteractiva").show();
        $("#videoAinteractiva").hide();
    }
}

//Funcion Init de aplicación
function init(){
	console.log("Iniciando AInteractiva");
}