#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 20 13:45:31 2018

@author: mars
"""

import sys

for linea in sys.stdin:
    linea = linea.strip()
    palabras = linea.split(" ")
    
    for palabra in palabras:
        print '%s\t%s' % (palabra, 1)