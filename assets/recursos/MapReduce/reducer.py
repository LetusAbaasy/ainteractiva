#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 20 13:53:05 2018

@author: mars
"""

import sys

item_actual = None
item_cont = 0

for linea in sys.stdin:
    linea = linea.strip()
    item, count = linea.split("\t")
    
    try:
        count = int(count)
    except ValueError:
        continue

    if item_actual == item:
        item_cont += count
    else:
        if item_actual: #primera
            print '%s\t%s' % (item_actual, item_cont)
        
        item_actual = item
        item_cont = count
    
if item_actual == item: #ultima
    print '%s\t%s' % (item_actual, item_cont)
