#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  5 20:15:32 2018

@author: mars
"""

import os
import sys
import pandas as pd
import numpy as np
import csv
from django.shortcuts import render
from pprint import pprint
from subprocess import call
#bokeh
from bokeh.plotting import figure, show, output_file
from bokeh.layouts import row, gridplot
from bokeh.io import output_file, show
from bokeh.palettes import Viridis3
from bokeh.transform import linear_cmap
from bokeh.util.hex import hexbin
from bokeh.models import LinearAxis, Range1d
#np
from numpy import pi, arange, sin, linspace
from operator import itemgetter


aranda = "aranda/"
cms = "cms/"
encuesta = "encuesta/"

datos = pd.read_csv(aranda+'arandaAbril2017.csv',delimiter=';',decimal=",",index_col=0)

datos.groupby('Categoria').count()

datos['Categoria'].tolist()
datos.sort_values(by=['Tipo Caso','Categoria'], ascending=[True,False])

import matplotlib.pyplot as plt

x=[]
y=[]
# coding=ISO-8859-1

with open('arandaMerge.csv', 'rb') as csvfile:
    reader = csv.DictReader(csvfile, delimiter=',')
    for data in reader:
        data['Registro']
        #plt.plot(data['Registro'])
        #plt.plot(data['Cod Caso'])
         #print(, row['Registro'], row['Estado'], )

plt.legend(loc='upper right')
plt.plot(figure_1,figure_2,label='csv')
plt.xlabel('Tipo Caso')
plt.ylabel('Categoria')
plt.title('pls let this work')
plt.legend()
plt.show()


fileNames = os.listdir(aranda)
archivos = []
fileNames = [file for file in fileNames if '.csv' in file]
for file in fileNames:
    archivos.append(pd.read_csv(aranda+file, delimiter=';',decimal=",",index_col=0))
merge = pd.concat(archivos)
merge.to_csv("arandaMerge.csv", index=False)


fileNames2 = os.listdir(cms)
archivos2 = []
fileNames2 = [file for file in fileNames2 if '.csv' in file]
for file in fileNames2:
    archivos2.append(pd.read_csv(cms+file, delimiter=';',decimal=",",index_col=0))
merge2 = pd.concat(archivos2)
merge2.to_csv("cmsMerge.csv", index=False)

import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt(aranda+file, delimiter=";", dtype=[("Tipo Caso", "f4"), ("column1", "f8"), ("column2", "f8")])
