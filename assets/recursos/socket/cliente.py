#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 19:20:54 2018

@author: mars
"""

import socket
import time
scliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
scliente.connect(('localhost', 5000))

print("Iniciando cliente...")

while True:
    time.sleep(2)
    menu = scliente.recv(1024)
    print(menu)
    mensaje = str(input("Cliente >>> "))
    scliente.send(mensaje.encode("utf-8"))
    recibido = scliente.recv(1024)
    if(recibido == 'b\'t\''):
        break
    print("Respuesta = ", recibido)
scliente.close()