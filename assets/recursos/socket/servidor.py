#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 19:11:24 2018

@author: mars
"""

import socket
import pandas as pd
import time

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('127.0.0.1',5000))
s.listen(5)

print("Iniciando el servidor...")

respuesta = ""
menu = str("\n Menu \n 1) Maximo \n 2) Minino \n 3) Desviacion \n 4) Suma \n t) Terminar \n")

datos_est=pd.read_csv('ejemplo_estudiantes.csv',delimiter=';',decimal=",",index_col=0)
datos_pandas = pd.DataFrame(datos_est)
maximo = datos_est.values.max()
minimo = datos_est.values.min()
desviacion = datos_est.mean()
suma = datos_est.sum()
mediana = datos_est.median()
moda = datos_est.mode()

datos = str(datos_est)

while True:
    #if(respuesta == 'b\'t\''):
     #   break
    sc, add = s.accept()
    print("Cliente conectado desde ",add,' Para terminar presione t')
    sc.send(menu.encode("utf-8"))
    sc.send(datos.encode("utf-8"))
    
    while True:
        recibido = sc.recv(1024)
        print("Recibido ", recibido)
        if(recibido == "1"):
            respuesta = str(maximo)
            sc.send(respuesta.encode("utf-8"))
        elif(recibido == "2"):
            respuesta = str(minimo)
            sc.send(respuesta.encode("utf-8"))
        elif(recibido == "3"):
            respuesta = str(desviacion)
            sc.send(respuesta.encode("utf-8"))
        elif(recibido == "4"):
            respuesta = str(suma)
            sc.send(respuesta.encode("utf-8"))
            
        if((recibido == 'b\'t\'') or (respuesta == 't')):
            break
        time.sleep(2)
        #mensaje = str("Servidor >>> "+recibido)
        mensaje = str("Ingrese otra opcion...")
        sc.send(mensaje.encode("utf-8"))
print("Finaliza la conversacion")
s.close()
sc.close()