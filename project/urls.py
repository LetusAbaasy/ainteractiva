"""atencion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r"^$", views.home, name="home"), #r"^$" para entrar a raiz / - r'^home/$'
    url(r'^bigdata/$', views.bigdata, name="bigdata"),
    url(r'^ejercicios/$', views.ejercicios, name="ejercicios"),
    url(r'^resultados/$', views.resultados, name="resultados"),
    #Aranda
    url(r'^arandaBarrasRegistro/$', views.arandaBarrasRegistro, name="arandaBarrasRegistro"),
    url(r'^arandaDonutCaso/$', views.arandaDonutCaso, name="arandaDonutCaso"),
    url(r'^arandaBarraTotal/$', views.arandaBarraTotal, name="arandaBarraTotal"),
    url(r'^arandaBarrasServicio/$', views.arandaBarrasServicio, name="arandaBarrasServicio"),
    #CMS
    url(r'^tiempo/$', views.tiempo, name="tiempo"),
    url(r'^tiempoTotal/$', views.tiempoTotal, name="tiempoTotal"),
    url(r'^tiempoTotalMinMidMax/$', views.tiempoTotalMinMidMax, name="tiempoTotalMinMidMax"),
    #Encuesta
    url(r'^encuestaResultadosAtencion/$', views.encuestaResultadosAtencion, name="encuestaResultadosAtencion"),
    url(r'^encuestaResultadosRespeto/$', views.encuestaResultadosRespeto, name="encuestaResultadosRespeto"),
    url(r'^encuestaResultadosEfectivo/$', views.encuestaResultadosEfectivo, name="encuestaResultadosEfectivo"),
    url(r'^encuestaResultadosVS/$', views.encuestaResultadosVS, name="encuestaResultadosVS"),
    url(r'^wordcloud/$', views.wordcloud, name="wordcloud"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
