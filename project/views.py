# coding=utf-8
import os
import sys
import random
import string
import pandas as pd
import numpy as np
import json
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import nltk
from math import pi
from django.shortcuts import render
from pprint import pprint
from subprocess import call
from bokeh.plotting import figure, show, output_file, reset_output
from bokeh.layouts import row, gridplot, layout
from bokeh.io import output_file, show
from bokeh.palettes import *
from bokeh.transform import *
from bokeh.util.hex import hexbin
from bokeh.models import *
from bokeh.models.widgets import Slider
from django.http import HttpResponseRedirect
from numpy import pi, arange, sin, linspace
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize
from collections import OrderedDict

#Vista Home
def home(request):
	datos = {
		"title":"Home"
	}

	return render(request, "home/home.html", datos)

#Vista big data
def bigdata(request):
	datos = {
		"title":"¿Qué es Big Data?"
	}

	return render(request, "home/bigdata.html", datos)

#Vista ejercicios
def ejercicios(request):
	#Variables plots
	nombre = "assets/img/plot.html";
	proceso = call("rm assets/img/plot.html", shell=True)
	output_file(nombre, title="Ejemplos de plots")
	#Para MapReduce
	archivo = open(os.path.join('assets/recursos/txt/', 'ejemploPalabras.txt'))
	leerArchivo = archivo.read()
	#Proceso 
	proceso = call("cat assets/recursos/txt/ejemploPalabras.txt | python assets/recursos/MapReduce/mapper.py | sort | python assets/recursos/MapReduce/reducer.py > assets/recursos/txt/salidaReduce.txt", shell=True)
	#Respuesta
	archivoRespuesta = open(os.path.join('assets/recursos/txt', 'salidaReduce.txt'))
	leerArchivoRespuesta = archivoRespuesta.read()
	# Para pandas
	datos_est = pd.read_csv('assets/recursos/csv/ejemplo_estudiantes.csv',delimiter=',',decimal=",",index_col=0)
	datos_pandas = pd.DataFrame(datos_est);
	#___
	minimoMateria = min(datos_est)
	maximoMateria = max(datos_est)
	maximo = datos_est.values.max()
	minimo = datos_est.values.min()
	desviacion = datos_est.mean()
	suma = datos_est.sum()
	mediana = datos_est.median()
	moda = datos_est.mode()
	lista = []
	#Ejemplos de bokeh
	s1 = figure(plot_width=400, plot_height=400)
	s1.line([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], line_width=2)
	#___
	s2 = figure(plot_width=400, plot_height=400)
	s2.step([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], line_width=2, mode="center")
	#___
	s3 = figure(plot_width=400, plot_height=400)
	s3.vbar(x=[1, 2, 3], width=0.5, bottom=0,
   		top=[1.2, 2.5, 3.7], color="firebrick")
	#___
	n = 50000
	x = np.random.standard_normal(n)
	y = np.random.standard_normal(n)
	bins = hexbin(x, y, 0.1)
	s4 = figure(tools="wheel_zoom,reset", match_aspect=True, background_fill_color='#440154')
	s4.grid.visible = False
	s4.hex_tile(q="q", r="r", size=0.1, line_color=None, source=bins,
	           fill_color=linear_cmap('counts', 'Viridis256', 0, max(bins.counts)))
	#__
	x = arange(-2*pi, 2*pi, 0.1)
	y = sin(x)
	y2 = linspace(0, 100, len(y))
	s5 = figure(x_range=(-6.5, 6.5), y_range=(-1.1, 1.1))
	s5.circle(x, y, color="orange")
	s5.extra_y_ranges = {"foo": Range1d(start=0, end=100)}
	s5.circle(x, y2, color="#1b78b5", y_range_name="foo")
	s5.add_layout(LinearAxis(y_range_name="foo"), 'left')
	# Array de datos a enviar a template
	datos = {
		"title":"Ejercicios",
		"datos":datos_est,
		"minimo":minimo,
		"minimoMateria":minimoMateria,
		"maximoMateria":maximoMateria,
		"maximo":maximo,
		"minimo":minimo,
		"desviacion":desviacion,
		"suma":suma,
		"mediana":mediana,
		"moda":moda,
		"leerArchivo":leerArchivo,
		"leerArchivoRespuesta":leerArchivoRespuesta,
		"plots":nombre
	}

	#Graficas a mostrar
	grid = gridplot([s1, s2, s3, s4, s5], ncols=3, plot_width=350, plot_height=280)
	show(grid)
	return render(request, "home/ejercicios.html", datos)

#Vista resultados
def resultados(request):
	aranda = os.listdir("assets/recursos/csv/aranda/")
	cms = os.listdir("assets/recursos/csv/cms/")
	encuesta = os.listdir("assets/recursos/csv/encuesta/")

	datos = {
		"title":"Resultados",
		"aranda":aranda,
		"cms":cms,
		"encuesta":encuesta,
	}

	return render(request, "home/resultados.html", datos)

#Grafica de barras de aranda por tipo de caso en registros total con comentarios
def arandaBarraTotal(request):
	#Variables
	resultadosAranda = []
	totalArandaRegistros = []
	totalIncidentes = 0
	totalRequerimientos = 0
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	aranda = os.listdir("assets/recursos/csv/aranda/")
	aranda = [file for file in aranda if '.csv' in file]
	rutaAranda = "assets/recursos/csv/aranda/"
	aranda = os.listdir(rutaAranda)
	#Ordenar registros
	aranda.sort()
	#ID Unico para cada grafica
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	#Url a mostrar
	url = str('../assets/img/resultados/'+unique_id+'.html')
	#HTML donde se mostraran las graficas
	resultadosAranda = "assets/img/resultados/"+unique_id+".html"
	#Eliminar archivos para espacio y verificar resultados correctos
	proceso = call("rm assets/img/resultados/*", shell=True)
	#Archivo de salida de Bokeh donde crea el HTML 
	output_file(resultadosAranda, title="Resultado Meses Aranda")
	#Nombres en lista
	series = ["Mail", "Telefono", "Web"]
	#for para procesamiento
	for file in aranda:
		#Leer archivo
		archivo = pd.read_csv(rutaAranda+file, usecols=['Tipo Caso', 'Registro'], encoding='latin1', delimiter=',') 
		#Localizacion de tipo de caso igual a incidentes
		incidentes = archivo.loc[archivo['Tipo Caso'] == 'INCIDENTES']
		#Agrupar por incidentes de en registros
		df = incidentes.groupby(['Registro']).size()
		#Localizacion de tipo de caso igual a requerimientos
		requerimientos = archivo.loc[archivo['Tipo Caso'] == 'REQUERIMIENTOS']
		#Agrupar por requerimientos
		df2 = requerimientos.groupby(['Registro']).size()
		#Tooltips de hover en grafica
		tooltip = [
    		('Valor', '$y'),
		]
		#Acumular resultados de los archivos
		totalIncidentes += df
		totalRequerimientos += df2

	x = {
		'Total de Incidentes': totalIncidentes.values.sum(),
		'Total de Requerimientos': totalRequerimientos.values.sum(),
	}

	# #Inicia figure de bokeh
	# unique_id = figure(x_range=series, plot_height=250, tooltips=tooltip, title="Total de incidentes vs requerimientos")
	# #Grafica de barra de bokeh
	# unique_id.vbar(x=series, top=list(totalRequerimientos), width=0.2, legend="Requerimientos", color="#1b78b5")
	# unique_id.vbar(x=series, top=list(totalIncidentes), width=0.2, legend="Incidentes", color="orange")
	# unique_id.xgrid.grid_line_color = None
	# unique_id.y_range.start = 0
	# unique_id.legend.orientation = "horizontal"
	# unique_id.legend.location = "top_center"
	# #Linea y puntos de bokeh
	# unique_id.line(x=["Mail", "Telefono", "Web"], y=list(totalRequerimientos), color="#1b78b5", line_width=2, legend="Requerimientos")
	# unique_id.circle(["Mail", "Telefono", "Web"], list(totalRequerimientos), fill_color="white", size=8)
	# unique_id.line(x=["Mail", "Telefono", "Web"], y=list(totalIncidentes), color="orange", line_width=2, legend="Incidentes")
	# unique_id.circle(["Mail", "Telefono", "Web"], list(totalIncidentes), fill_color="white", size=8)
	source = ColumnDataSource(data=dict(names=['Total de Incidentes']))
	source2 = ColumnDataSource(data=dict(names=['Total de Requerimientos']))
	data = pd.Series(x).reset_index(name='value').rename(columns={'index':'tipo'})
	data['angle'] = data['value']/data['value'].sum()*2*pi

	unique_id = figure(plot_height=450, plot_width=950, title="Tipos de solicitud", toolbar_location=None, tools="hover", tooltips="@tipo: @value", x_range=(-0.5, 1.0))
	unique_id.wedge(x=0, y=1, radius=0.4,start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),line_color="white", legend='Tipos de solicitud', source=data, color="orange")
	labels = LabelSet(text='names', level='glyph',x_offset=90, y_offset=300, source=source, render_mode='canvas')
	labels2 = LabelSet(text='names', level='glyph',x_offset=-50, y_offset=100, source=source2, render_mode='canvas')
	unique_id.add_layout(labels)
	unique_id.add_layout(labels2)

	#tab = Panel(child=unique_id, title="Tipos de solicitud")
	#tabs = Tabs(tabs=[ tab1, tab2 ])

	#añadir a un array los resultados
	totalArandaRegistros.append(unique_id)
	#Si se pone en grid
	gridtotalArandaRegistros = gridplot(totalArandaRegistros, ncols=1, plot_width=1000, plot_height=600)
	#Muestra el resultado
	show(gridtotalArandaRegistros)
	#Return a redireccion
	reset_output()
	return HttpResponseRedirect(url)

#Graficas por mes de cada archivo en aranda
def arandaBarrasRegistro(request):
	resultadosAranda = []
	totalArandaRegistrosIncidentes = []
	totalArandaRegistrosRequerimientos = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	aranda = os.listdir("assets/recursos/csv/aranda/")
	aranda = [file for file in aranda if '.csv' in file]
	rutaAranda = "assets/recursos/csv/aranda/"
	aranda = os.listdir(rutaAranda)
	aranda.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosAranda = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosAranda, title="Resultado Meses Aranda")
	series = ["Mail", "Telefono", "Web"]
	for file in aranda:
		archivo = pd.read_csv(rutaAranda+file, usecols=['Tipo Caso', 'Registro'], encoding='latin1', delimiter=',') 
		incidentes = archivo.loc[archivo['Tipo Caso'] == 'INCIDENTES']
		df = incidentes.groupby(['Registro']).size()
		requerimientos = archivo.loc[archivo['Tipo Caso'] == 'REQUERIMIENTOS']
		df2 = requerimientos.groupby(['Registro']).size()

		# unique_id = figure(x_range=series, plot_height=250, tooltips=tooltip, title="Tipo registros en "+file)
		# unique_id.vbar(x=series, top=list(df2), width=0.2, legend="Requerimientos", color="#1b78b5")
		# unique_id.vbar(x=series, top=list(df), width=0.2, legend="Incidentes", color="orange")
		# unique_id.xgrid.grid_line_color = None
		# unique_id.y_range.start = 0
		# unique_id.legend.orientation = "horizontal"
		# unique_id.legend.location = "top_center"
		# unique_id.line(x=["Mail", "Telefono", "Web"], y=list(df2), color="#1b78b5", line_width=2, legend="Requerimientos")
		# unique_id.circle(["Mail", "Telefono", "Web"], list(df2), fill_color="white", size=8)
		# unique_id.line(x=["Mail", "Telefono", "Web"], y=list(df), color="orange", line_width=2, legend="Incidentes")
		# unique_id.circle(["Mail", "Telefono", "Web"], list(df), fill_color="white", size=8)

		x = {
			'Correo': df["Mail"],
			'Telefono': df["Telefono"],
			'Web': df["Web"],
		}

		x2 = {
			'Correo': df2["Mail"],
			'Telefono': df2["Telefono"],
			'Web': df2["Web"],
		}

		data = pd.Series(x).reset_index(name='value').rename(columns={'index':'incidentes'})
		data['angle'] = data['value']/data['value'].sum()*2*pi

		unique_id = figure(plot_height=650, plot_width=800, title=file, toolbar_location=None,tools="hover", tooltips="@incidentes: @value", x_range=(-0.5, 1.0))
		unique_id.wedge(x=0, y=1, radius=0.4,start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),line_color="white", legend='Incidentes', source=data, color="navy")
		totalArandaRegistrosIncidentes.append(Panel(child=unique_id, title=file))        
		tabs1 = Tabs(tabs=totalArandaRegistrosIncidentes)

		data2 = pd.Series(x2).reset_index(name='value').rename(columns={'index':'requerimientos'})
		data2['angle'] = data2['value']/data2['value'].sum()*2*pi

		unique_id = figure(plot_height=650, plot_width=800, title=file, toolbar_location=None,tools="hover", tooltips="@requerimientos: @value", x_range=(-0.5, 1.0))
		unique_id.wedge(x=0, y=1, radius=0.4,start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),line_color="white", legend='Requerimientos', source=data2, color="orange")

		totalArandaRegistrosRequerimientos.append(Panel(child=unique_id, title=file))        
		tabs2 = Tabs(tabs=totalArandaRegistrosRequerimientos)

	#totalArandaRegistros.append(unique_id)
	gridtotalArandaRegistros = gridplot([tabs1, tabs2], ncols=2, plot_width=800, plot_height=1920, sizing_mode='stretch_both')

	show(gridtotalArandaRegistros)
	reset_output()
	return HttpResponseRedirect(url)

#Grafica de dona de aranda por tipo de caso en registro total
def arandaDonutCaso(request):
	resultadosAranda = []
	totalArandaRegistros = []
	totalIndi = 0
	totalRq = 0
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	aranda = os.listdir("assets/recursos/csv/aranda/")
	aranda = [file for file in aranda if '.csv' in file]
	rutaAranda = "assets/recursos/csv/aranda/"
	aranda = os.listdir(rutaAranda)
	aranda.sort()
	resultadosAranda = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosAranda, title="Resultado Meses Aranda")

	for file in aranda:
		datosTP = []
		datosRQ = []

		with open(rutaAranda+file) as File:
			lineas = File.read().splitlines()
			lineas.pop(0)

			textoTP ="INCIDENTES"
			for l in lineas:
				linea=l.split(';')
				if textoTP == linea[0]:
					datosTP.append(linea[0])
			canTP = len(datosTP)

			textoRQ ="REQUERIMIENTOS"
			for l in lineas:
				linea=l.split(';')
				if textoRQ == linea[0]:
					datosRQ.append(linea[0])
			canRQ = len(datosRQ)

		totalIndi += canTP
		totalRq += canRQ

	x = {
		'Indicentes': totalIndi,
		'Requerimientos': totalRq
	}

	data = pd.Series(x).reset_index(name='value').rename(columns={'index':'caso'})
	data['angle'] = data['value']/data['value'].sum()*2*pi

	unique_id = figure(plot_height=350, title="Dona de Requerimientos vs Indicentes", toolbar_location=None,
		tools="hover", tooltips="@caso: @value", x_range=(-0.5, 1.0))

	unique_id.wedge(x=0, y=1, radius=0.4,
		start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
		line_color="white", legend='caso', source=data)

	totalArandaRegistros.append(unique_id)
	gridtotalArandaRegistros = gridplot(totalArandaRegistros, ncols=1, plot_width=1250, plot_height=800)

	show(gridtotalArandaRegistros)

	reset_output()
	return HttpResponseRedirect(url)


def arandaBarrasServicio(request):
	resultadosAranda = []
	totalArandaRegistrosIncidentes = []
	totalArandaRegistrosRequerimientos = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	aranda = os.listdir("assets/recursos/csv/aranda/")
	aranda = [file for file in aranda if '.csv' in file]
	rutaAranda = "assets/recursos/csv/aranda/"
	aranda = os.listdir(rutaAranda)
	aranda.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosAranda = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosAranda, title="Resultado Meses Aranda")

	seriesIncidentes = ["CRM", "Directorio Activo", "Elementos de Microinformática", 
		"Impresión", "Internet", "Office 365", "Portales Web", 
		"Administración Aranda", "Conectividad de Redes", 
		"Control de Acceso Biometrico", "Correo Electrónico", 
		"Soporte Aranda", "Servicio de Telefóno", "Soporte de Aplicaciones"]

	seriesRequerimientos = ["CRM", "Dir. Activo", "Elementos de Microinformática", 
		"Impresión", "Internet", "Office 365", "SAP ERP", "Conectividad de Redes", 
		"Conexión por VPN", "Acceso Biometrico", 
		"Correo Electrónico", "Repositorio Compartido", 
		"Soporte Antivirus", "Soporte Aranda", 
		"Servicio de Telefóno", "VideoConferencia", 
		"Soporte de Aplicaciones"]

	tooltip = [
		('Valor', '$y'),
	]
	for file in aranda:
		archivo = pd.read_csv(rutaAranda+file, usecols=['Tipo Caso', 'Servicio'], encoding='latin1', delimiter=',') 
		incidentes = archivo.loc[archivo['Tipo Caso'] == 'INCIDENTES']
		df = incidentes.groupby(['Servicio']).size()

	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	unique_id = figure(x_range=seriesIncidentes, plot_height=580, plot_width=1720, tooltips=tooltip, title="Tipo servicio Indicentes")
	unique_id.vbar(x=seriesIncidentes, top=list(df), width=0.2, legend="Incidentes",  color="#1b78b5")
	unique_id.xgrid.grid_line_color = None
	unique_id.y_range.start = 0
	unique_id.legend.orientation = "horizontal"
	unique_id.legend.location = "top_center"
	#unique_id.line(x=seriesIncidentes, y=list(df), color="#1b78b5", line_width=2, legend="Incidentes")
	#unique_id.circle(seriesIncidentes, list(df), fill_color="white", size=8)
	totalArandaRegistrosIncidentes.append(Panel(child=unique_id, title="Barras de tipo de Incidentes"))        

	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	data = pd.Series(df).reset_index(name='value').rename(columns={'index':'incidentes'})
	data['angle'] = data['value']/data['value'].sum()*2*pi

	unique_id = figure(plot_height=580, plot_width=850, title="Tipos de Incidentes", toolbar_location=None,
		tools="hover", tooltips="@incidentes: @value", x_range=(-0.5, 1.0))

	unique_id.wedge(x=0, y=1, radius=0.4,
		start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
		line_color="white", legend='Incidentes', source=data)

	totalArandaRegistrosIncidentes.append(Panel(child=unique_id, title="Dona de tipos de Incidentes"))        
	tabs1 = Tabs(tabs=totalArandaRegistrosIncidentes)


	#totalArandaRegistros.append(unique_id)

	for file in aranda:
		archivo = pd.read_csv(rutaAranda+file, usecols=['Tipo Caso', 'Servicio'], encoding='latin1', delimiter=',')
		requerimientos = archivo.loc[archivo['Tipo Caso'] == 'REQUERIMIENTOS']
		df2 = requerimientos.groupby(['Servicio']).size()

	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	unique_id = figure(x_range=seriesRequerimientos, plot_height=580,  plot_width=1720, tooltips=tooltip, title="Tipo servicio Requerimientos")
	unique_id.vbar(x=seriesRequerimientos, top=list(df2), width=0.2, legend="Requerimientos",  color="orange")
	unique_id.xgrid.grid_line_color = None
	unique_id.y_range.start = 0
	unique_id.legend.orientation = "horizontal"
	unique_id.legend.location = "top_center"
	#unique_id.line(x=seriesRequerimientos, y=list(df2), color="orange", line_width=2, legend="Requerimientos")
	#unique_id.circle(seriesRequerimientos, list(df2), fill_color="white", size=8)
	totalArandaRegistrosRequerimientos.append(Panel(child=unique_id, title="Barras de tipos de Requerimientos"))        

	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	data = pd.Series(df2).reset_index(name='value').rename(columns={'index':'incidentes'})
	data['angle'] = data['value']/data['value'].sum()*2*pi

	unique_id = figure(width=850, height=580, title="Tipo de Requerimientos", toolbar_location=None,
		tools="hover", tooltips="@incidentes: @value", x_range=(-0.5, 1.0))

	unique_id.wedge(x=0, y=1, radius=0.4,
		start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
		line_color="white", legend='Requerimientos', source=data,  color="orange")

	totalArandaRegistrosRequerimientos.append(Panel(child=unique_id, title="Dona de tipos de Requerimientos"))        
	tabs2 = Tabs(tabs=totalArandaRegistrosRequerimientos)
	#totalArandaRegistros.append(unique_id)

	gridtotalArandaRegistros = gridplot([tabs1, tabs2], ncols=1, plot_width=1720, plot_height=680, sizing_mode='fixed')
	#gridtotalArandaRegistros = gridplot(totalArandaRegistros, ncols=1, plot_width=1280, plot_height=400)
	show(gridtotalArandaRegistros)
	reset_output()
	return HttpResponseRedirect(url)

def wordcloud(request):
	#nltk.download('spanish')
	#nltk.download('stopwords')
	#nltk.download('punkt')
	resultadosEncuesta = []
	totalEncuestaRegistros = []
	filtered_sentence = [] 
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	encuesta = os.listdir("assets/recursos/csv/encuesta/")
	encuesta = [file for file in encuesta if '.csv' in file]
	rutaencuesta = "assets/recursos/csv/encuesta/"
	encuesta = os.listdir(rutaencuesta)
	encuesta.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.png')
	url2 = str('/home/ubuntu/ainteractiva/assets/img/resultados/'+unique_id+'.png')
	resultadosEncuesta = "assets/img/resultados/"+unique_id+".png"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosEncuesta, title="WordCloud")
	stop_words = set(stopwords.words('spanish'))
	listaNueva = ['u00f3n', 'u00f3', 'u00e9', 'u00eda', 'u00e1', 'nEl', 'nGracias', 'u00edas', 
		'u00e9cnico', 'u00edticas', 'conexi', 'u00ed', 'u00fan', 'aplica', 'u00e1s', 'opci', 'nEn', 'nDecripci', 'creaci', 'soluci', 'nAdemas']
	nuevaLista = stop_words.union(listaNueva)

	for file in encuesta:
		archivo = pd.read_csv(rutaencuesta+file, usecols=['Sugerencias'], encoding='latin1', delimiter=';') 
		incidentes = archivo.loc[archivo['Sugerencias'] != "NaN"]
		df = incidentes.groupby(['Sugerencias']).size()

	texto = json.dumps(df.index.values.tolist())
	word_tokens = word_tokenize(texto)
	filtered_sentence = [w for w in word_tokens if not w in stop_words] 

	for w in word_tokens: 
		if w not in stop_words: 
			filtered_sentence.append(w)

	wordcloud = WordCloud(width=1500, height=600, max_font_size=55, max_words=800, background_color="white", collocations=False, repeat=True, stopwords=nuevaLista).generate(texto)
	unique_id = figure(x_range=(0,1), y_range=(0,1))
	plt.figure(figsize=(15,6), facecolor='k')
	plt.imshow(wordcloud, interpolation="bilinear")
	plt.axis("off")
	plt.savefig(url2)
	#plt.show()
	plt.close("all")
	#unique_id.image_url(url=plt.show(), x=0, y=1)

	#totalArandaRegistros.append(unique_id)
	
	#gridtotalArandaRegistros = gridplot(totalArandaRegistros, ncols=1, plot_width=1280, plot_height=750)
	#show(gridtotalArandaRegistros)
	#reset_output()
   	return HttpResponseRedirect(url)

#Graficas por encuesta
def encuestaResultadosAtencion(request):
	resultadosEncuesta = []
	totalEncuestaRegistros = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	encuesta = os.listdir("assets/recursos/csv/encuesta/")
	encuesta = [file for file in encuesta if '.csv' in file]
	rutaencuesta = "assets/recursos/csv/encuesta/"
	encuesta = os.listdir(rutaencuesta)
	encuesta.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosEncuesta = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosEncuesta, title="Resultado Meses encuesta")
	series = ["1. Regular", "2. Bueno", "3. Excelente"]
	for file in encuesta:
		archivo = pd.read_csv(rutaencuesta+file, usecols=['Tipo', 'Especialista', 'Respeto', 'Efectivo', 'Atencion'], encoding='latin1', delimiter=';') 
		incidentes = archivo.loc[archivo['Tipo'] == 'Incidente']
		df = incidentes.groupby(['Atencion']).size()
		requerimientos = archivo.loc[archivo['Tipo'] == 'Requerimientos']
		df2 = requerimientos.groupby(['Atencion']).size()
		tooltip = [
    		('Valor', '$y'),
		]
		unique_id = figure(x_range=series, plot_height=250, tooltips=tooltip, title="Encuesta basada en Atencion")
		unique_id.vbar(x=series, top=list(df2), width=0.2, legend="Requerimientos", color="#1b78b5")
		unique_id.vbar(x=series, top=list(df), width=0.2, legend="Incidentes", color="orange")
		unique_id.xgrid.grid_line_color = None
		unique_id.y_range.start = 0
		unique_id.legend.orientation = "horizontal"
		unique_id.legend.location = "top_center"
		unique_id.line(x=["1. Regular", "2. Bueno", "3. Excelente"], y=list(df2), color="#1b78b5", line_width=2, legend="Requerimientos")
		unique_id.circle(["1. Regular", "2. Bueno", "3. Excelente"], list(df2), fill_color="white", size=8)
		unique_id.line(x=["1. Regular", "2. Bueno", "3. Excelente"], y=list(df), color="orange", line_width=2, legend="Incidentes")
		unique_id.circle(["1. Regular", "2. Bueno", "3. Excelente"], list(df), fill_color="white", size=8)
		totalEncuestaRegistros.append(unique_id)
		gridtotalEncuestaRegistros = gridplot(totalEncuestaRegistros, ncols=1, plot_width=1600, plot_height=728)
	show(gridtotalEncuestaRegistros)
	reset_output()
	return HttpResponseRedirect(url)

def encuestaResultadosRespeto(request):
	resultadosEncuesta = []
	totalEncuestaRegistros = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	encuesta = os.listdir("assets/recursos/csv/encuesta/")
	encuesta = [file for file in encuesta if '.csv' in file]
	rutaencuesta = "assets/recursos/csv/encuesta/"
	encuesta = os.listdir(rutaencuesta)
	encuesta.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosEncuesta = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosEncuesta, title="Resultado Meses encuesta")
	series = ["1. Regular", "2. Bueno", "3. Excelente"]
	for file in encuesta:
		archivo = pd.read_csv(rutaencuesta+file, usecols=['Tipo', 'Especialista', 'Respeto', 'Efectivo', 'Atencion'], encoding='latin1', delimiter=';') 
		incidentes = archivo.loc[archivo['Tipo'] == 'Incidente']
		df = incidentes.groupby(['Respeto']).size()
		requerimientos = archivo.loc[archivo['Tipo'] == 'Requerimientos']
		df2 = requerimientos.groupby(['Respeto']).size()
		tooltip = [
    		('Valor', '$y'),
		]
		unique_id = figure(x_range=series, plot_height=250, tooltips=tooltip, title="Encuesta basada en Respeto")
		unique_id.vbar(x=series, top=list(df2), width=0.2, legend="Requerimientos", color="#1b78b5")
		unique_id.vbar(x=series, top=list(df), width=0.2, legend="Incidentes", color="orange")
		unique_id.xgrid.grid_line_color = None
		unique_id.y_range.start = 0
		unique_id.legend.orientation = "horizontal"
		unique_id.legend.location = "top_center"
		unique_id.line(x=["1. Regular", "2. Bueno", "3. Excelente"], y=list(df2), color="#1b78b5", line_width=2, legend="Requerimientos")
		unique_id.circle(["1. Regular", "2. Bueno", "3. Excelente"], list(df2), fill_color="white", size=8)
		unique_id.line(x=["1. Regular", "2. Bueno", "3. Excelente"], y=list(df), color="orange", line_width=2, legend="Incidentes")
		unique_id.circle(["1. Regular", "2. Bueno", "3. Excelente"], list(df), fill_color="white", size=8)
		totalEncuestaRegistros.append(unique_id)
		gridtotalEncuestaRegistros = gridplot(totalEncuestaRegistros, ncols=1, plot_width=1600, plot_height=728)
	show(gridtotalEncuestaRegistros)
	reset_output()
	return HttpResponseRedirect(url)

def encuestaResultadosEfectivo(request):
	resultadosEncuesta = []
	totalEncuestaRegistros = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	encuesta = os.listdir("assets/recursos/csv/encuesta/")
	encuesta = [file for file in encuesta if '.csv' in file]
	rutaencuesta = "assets/recursos/csv/encuesta/"
	encuesta = os.listdir(rutaencuesta)
	encuesta.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosEncuesta = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosEncuesta, title="Resultado Meses encuesta")
	series = ["1. Regular", "2. Bueno", "3. Excelente"]
	for file in encuesta:
		archivo = pd.read_csv(rutaencuesta+file, usecols=['Tipo', 'Especialista', 'Respeto', 'Efectivo', 'Atencion'], encoding='latin1', delimiter=';') 
		incidentes = archivo.loc[archivo['Tipo'] == 'Incidente']
		df = incidentes.groupby(['Efectivo']).size()
		requerimientos = archivo.loc[archivo['Tipo'] == 'Requerimientos']
		df2 = requerimientos.groupby(['Efectivo']).size()
		tooltip = [
    		('Valor', '$y'),
		]
		unique_id = figure(x_range=series, plot_height=250, tooltips=tooltip, title="Encuesta basada en Efectivo")
		unique_id.vbar(x=series, top=list(df2), width=0.2, legend="Requerimientos", color="#1b78b5")
		unique_id.vbar(x=series, top=list(df), width=0.2, legend="Incidentes", color="orange")
		unique_id.xgrid.grid_line_color = None
		unique_id.y_range.start = 0
		unique_id.legend.orientation = "horizontal"
		unique_id.legend.location = "top_center"
		unique_id.line(x=["1. Regular", "2. Bueno", "3. Excelente"], y=list(df2), color="#1b78b5", line_width=2, legend="Requerimientos")
		unique_id.circle(["1. Regular", "2. Bueno", "3. Excelente"], list(df2), fill_color="white", size=8)
		unique_id.line(x=["1. Regular", "2. Bueno", "3. Excelente"], y=list(df), color="orange", line_width=2, legend="Incidentes")
		unique_id.circle(["1. Regular", "2. Bueno", "3. Excelente"], list(df), fill_color="white", size=8)
		totalEncuestaRegistros.append(unique_id)
		gridtotalEncuestaRegistros = gridplot(totalEncuestaRegistros, ncols=1, plot_width=1600, plot_height=728)
	show(gridtotalEncuestaRegistros)
	reset_output()
	return HttpResponseRedirect(url)

def encuestaResultadosVS(request):
	resultadosEncuesta = []
	totalEncuestaRegistros = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	encuesta = os.listdir("assets/recursos/csv/encuesta/")
	encuesta = [file for file in encuesta if '.csv' in file]
	rutaencuesta = "assets/recursos/csv/encuesta/"
	encuesta = os.listdir(rutaencuesta)
	encuesta.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosEncuesta = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosEncuesta, title="Resultado Meses encuesta")
	series = ["Atencion", "Efectivo", "Respeto"]
	for file in encuesta:
		archivo = pd.read_csv(rutaencuesta+file, usecols=['Tipo', 'Especialista', 'Respeto', 'Efectivo', 'Atencion'], encoding='latin1', delimiter=';') 
		
		atencion = archivo.loc[archivo['Atencion'] == '3. Excelente']
		df = atencion.groupby(['Atencion']).size()

		atencion3 = archivo.loc[archivo['Atencion'] == '1. Regular']
		df22 = atencion3.groupby(['Atencion']).size()

		atencion2 = archivo.loc[archivo['Atencion'] == '2. Bueno']
		df11 = atencion2.groupby(['Atencion']).size()

		totalAtencion = (df.values+df22.values+df11.values)
		totalAtencionExcelente = (df.values*100)/totalAtencion
		totalAtencionRegular = (df22.values*100)/totalAtencion
		totalAtencionBueno = (df11.values*100)/totalAtencion
		totA = (totalAtencionExcelente+totalAtencionRegular+totalAtencionBueno)

		efectivo = archivo.loc[archivo['Efectivo'] == '3. Excelente']
		df2 = efectivo.groupby(['Efectivo']).size()

		efectivo3 = archivo.loc[archivo['Efectivo'] == '1. Regular']
		df33 = efectivo3.groupby(['Efectivo']).size()

		efectivo2 = archivo.loc[archivo['Efectivo'] == '2. Bueno']
		df22 = efectivo2.groupby(['Efectivo']).size()


		totalEfectivo = (df2.values+df33.values+df22.values)
		totalEfectivoExcelente = (df2.values*100)/totalEfectivo
		totalEfectivoRegular = (df33.values*100)/totalEfectivo
		totalEfectivoBueno = (df22.values*100)/totalEfectivo
		totE = (totalEfectivoExcelente+totalEfectivoRegular+totalEfectivoBueno)

		respeto = archivo.loc[archivo['Respeto'] == '3. Excelente']
		df3 = respeto.groupby(['Respeto']).size()

		respeto2 = archivo.loc[archivo['Respeto'] == '2. Bueno']
		df33 = respeto2.groupby(['Respeto']).size()

		respeto3 = archivo.loc[archivo['Respeto'] == '1. Regular']
		df44 = respeto3.groupby(['Respeto']).size()

		totalRespeto = (df3.values+df33.values+df44.values)
		totalRespetoExcelente = (df3.values*100)/totalRespeto
		totalRespetoRegular = (df33.values*100)/totalRespeto
		totalRespetoBueno = (df44.values*100)/totalRespeto
		totR = (totalRespetoExcelente+totalRespetoRegular+totalRespetoBueno)

		tooltip = [
    		('Valor', '$y'),
		]
		unique_id = figure(x_range=series, plot_height=250, tooltips=tooltip, title="Encuesta VS con tipos de evaluación")
		unique_id.vbar(x=series, top=[totR, totE, totA], width=0.2, legend="100 %", color="gray")
		unique_id.vbar(x=series, top=[totalRespetoExcelente, totalEfectivoExcelente, totalAtencionExcelente], width=0.2, legend="3. Excelente", color="orange")
		unique_id.vbar(x=series, top=[totalRespetoBueno, totalEfectivoBueno, totalAtencionBueno], width=0.2, legend="2. Bueno", color="#1b78b5")
		unique_id.vbar(x=series, top=[totalRespetoRegular, totalEfectivoRegular, totalAtencionRegular], width=0.2, legend="1. Regular", color="green")
		unique_id.xgrid.grid_line_color = None
		unique_id.y_range.start = 0
		unique_id.legend.orientation = "horizontal"
		unique_id.legend.location = "top_center"
		#unique_id.line(x=["Atencion", "Efectivo", "Respeto"], y=[totalAtencionBueno,totalEfectivoBueno,totalRespetoBueno], color="orange", line_width=2, legend="1. Regular")
		#unique_id.line(x=["Atencion", "Efectivo", "Respeto"], y=[totalAtencionRegular,totalEfectivoRegular,totalRespetoRegular], color="green", line_width=2, legend="2. Bueno")
		#unique_id.line(x=["Atencion", "Efectivo", "Respeto"], y=[totalAtencionExcelente,totalEfectivoExcelente,totalRespetoExcelente], color="#1b78b5", line_width=2, legend="3. Excelente")
		#unique_id.circle(["Atencion", "Efectivo", "Respeto"], [list(df), list(df2), list(df3)], fill_color="white", size=8)
		#unique_id.circle(["Atencion", "Efectivo", "Respeto"], [list(df11), list(df22), list(df33)], fill_color="white", size=8)
		#unique_id.circle(["Atencion", "Efectivo", "Respeto"], [list(df22), list(df33), list(df44)], fill_color="yellow", size=8)
		totalEncuestaRegistros.append(unique_id)
		gridtotalEncuestaRegistros = gridplot(totalEncuestaRegistros, ncols=1, plot_width=1600, plot_height=728)
	show(gridtotalEncuestaRegistros)
	reset_output()
	return HttpResponseRedirect(url)

def tiempo(request):
	resultadosTiempo = []
	totalTiempoReg = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	cms = os.listdir("assets/recursos/csv/cms/")
	cms = [file for file in cms if '.csv' in file]
	rutacms = "assets/recursos/csv/cms/"
	cms = os.listdir(rutacms)
	cms.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosTiempo = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosTiempo, title="Resultado Meses cms")
	for file in cms:
		archivo = pd.read_csv(rutacms+file, usecols=['Fecha', 'Llamadas Recibidas', 'Llamadas Atendidas'], encoding='latin1', delimiter=',') 
		#fecha = json.dumps(list(archivo["Fecha"].values))
		#llamadasR = json.dumps(list(archivo["Llamadas Recibidas"].values))
		# df = incidentes.groupby(['Efectivo']).size()
		# requerimientos = archivo.loc[archivo['Tipo'] == 'Requerimientos']
		# df2 = requerimientos.groupby(['Efectivo']).size()
		tooltip = [
			('Valor', '$y'),
		]

		unique_id = figure(x_range=archivo["Fecha"].values, plot_height=680, plot_width=1420, tooltips=tooltip, title="Llamadas Recibidas vs Llamadas Atendidas")
		unique_id.vbar(x=archivo["Fecha"].values, top=archivo["Llamadas Recibidas"].values, width=0.2, legend="Llamadas Recibidas", color="#1b78b5")
		unique_id.vbar(x=archivo["Fecha"].values, top=archivo["Llamadas Atendidas"].values, width=0.2, legend="Llamadas Atendidas", color="orange")
		#unique_id.line(x=archivo["Fecha"].values, y=archivo["Llamadas Recibidas"].values, color="#1b78b5", line_width=2, legend="Llamadas Recibidas")
		#unique_id.circle(archivo["Fecha"].values, archivo["Llamadas Recibidas"].values, fill_color="white", size=8)
		#unique_id.line(x=archivo["Fecha"].values, y=archivo["Llamadas Atendidas"].values, color="orange", line_width=2, legend="Llamadas Atendidas")
		#unique_id.circle(archivo["Fecha"].values, archivo["Llamadas Atendidas"].values, fill_color="white", size=8)
		unique_id.xgrid.grid_line_color = None
		unique_id.y_range.start = 0
		unique_id.legend.orientation = "horizontal"
		unique_id.legend.location = "top_center"

		totalTiempoReg.append(Panel(child=unique_id, title=file))        
		tabs1 = Tabs(tabs=totalTiempoReg)

	gridtotalTiempo = gridplot([tabs1], ncols=1, plot_width=680, plot_height=1420, sizing_mode='stretch_both')
	#totalTiempoReg.append(unique_id)
	#gridtotalTiempo = gridplot(totalTiempoReg, ncols=1, plot_width=1600, plot_height=728)
	show(gridtotalTiempo)
	reset_output()
	return HttpResponseRedirect(url)

def tiempoTotal(request):
	resultadosTiempo = []
	totalTiempoReg = []
	valoresR = []
	valoresA = []
	meses = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	cms = os.listdir("assets/recursos/csv/cms/")
	cms = [file for file in cms if '.csv' in file]
	rutacms = "assets/recursos/csv/cms/"
	cms = os.listdir(rutacms)
	cms.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosTiempo = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosTiempo, title="Resultado Meses cms")
	for file in cms:
		archivo = pd.read_csv(rutacms+file, usecols=['Fecha', 'Llamadas Recibidas', 'Llamadas Atendidas'], encoding='latin1', delimiter=',') 

		valoresR.append(archivo["Llamadas Recibidas"].mean())
		valoresA.append(archivo["Llamadas Atendidas"].mean())
		meses.append(file);
	tooltip = [
		('Valor', '$y'),
	]

	unique_id = figure(x_range=meses, plot_height=250, tooltips=tooltip, title="Media de Llamadas Recibidas vs Llamadas Atendidas")
	unique_id.vbar(x=meses, top=valoresR, width=0.2, legend="Llamadas Recibidas", color="#1b78b5")
	unique_id.vbar(x=meses, top=valoresA, width=0.2, legend="Llamadas Atendidas", color="orange")
	unique_id.xgrid.grid_line_color = None
	unique_id.y_range.start = 0
	unique_id.legend.orientation = "horizontal"
	unique_id.legend.location = "top_center"
	totalTiempoReg.append(unique_id)
	gridtotalTiempo = gridplot(totalTiempoReg, ncols=1, plot_width=1600, plot_height=728)
	show(gridtotalTiempo)
	reset_output()
	return HttpResponseRedirect(url)

def tiempoTotalMinMidMax(request):
	resultadosTiempo = []
	totalTiempoReg = []
	valoresminR = []
	valoresminA = []
	valoresmidR = []
	valoresmidA = []
	valoresmaxR = []
	valoresmaxA = []
	meses = []
	caracteresNombre = ''.join((string.ascii_letters, string.digits))
	cms = os.listdir("assets/recursos/csv/cms/")
	cms = [file for file in cms if '.csv' in file]
	rutacms = "assets/recursos/csv/cms/"
	cms = os.listdir(rutacms)
	cms.sort()
	unique_id = ''.join(random.choice(caracteresNombre) for _ in range(10))
	url = str('../assets/img/resultados/'+unique_id+'.html')
	resultadosTiempo = "assets/img/resultados/"+unique_id+".html"
	proceso = call("rm assets/img/resultados/*", shell=True)
	output_file(resultadosTiempo, title="Resultado Meses cms")
	for file in cms:
		archivo = pd.read_csv(rutacms+file, usecols=['Fecha', 'Llamadas Recibidas', 'Llamadas Atendidas'], encoding='latin1', delimiter=',') 

		valoresminR.append(archivo["Llamadas Recibidas"].min())
		valoresminA.append(archivo["Llamadas Atendidas"].min())

		valoresmidR.append(archivo["Llamadas Recibidas"].mean())
		valoresmidA.append(archivo["Llamadas Atendidas"].mean())

		valoresmaxR.append(archivo["Llamadas Recibidas"].max())
		valoresmaxA.append(archivo["Llamadas Atendidas"].max())

		meses.append(file);

	# inc = valoresminR > valoresmaxR
	# dec = valoresmaxR > valoresminR
	# w = 99

	# unique_id = figure(plot_width=1000, title = "MSFT Candlestick")
	# unique_id.xaxis.major_label_orientation = pi/4
	# unique_id.grid.grid_line_alpha=0.3

	# unique_id.segment(meses, valoresmaxR, meses, valoresminR, color="black")
	# unique_id.vbar(meses[inc], w, valoresmaxR[inc], valoresminR[inc], fill_color="#D5E1DD", line_color="black")
	# unique_id.vbar(meses[dec], w, valoresmaxR[dec], valoresminR[dec], fill_color="#F2583E", line_color="black")
	
	#unique_id = figure(plot_height = 600, plot_width = 600, title = 'Histogram of Arrival Delays', x_axis_label = 'Delay (min)]', y_axis_label = 'Number of Flights')
	tooltip = [
		('Valor', '$y'),
	]
	unique_id = figure(x_range=meses, plot_height=250, tooltips=tooltip, title="Comparación de Llamadas Recibidas vs Llamadas Atendidas")
	unique_id.vbar(x=meses, top=valoresmidR, width=0.2, legend="Media de Llamadas Recibidas", color="orange")
	unique_id.vbar(x=meses, top=valoresmidA, width=0.2, legend="Media de Llamadas Atendidas", color="#1b78b5")
	unique_id.xgrid.grid_line_color = None
	unique_id.y_range.start = 0
	unique_id.legend.orientation = "horizontal"
	unique_id.legend.location = "top_center"
	unique_id.line(x=meses, y=valoresmaxR, color="#ff8888", line_width=4, legend="Maximo de Llamadas Recibidas")
	unique_id.line(x=meses, y=valoresmaxA, color="blue", line_width=4, legend="Maximo de Llamadas Atendidas")
	unique_id.line(x=meses, y=valoresminR, color="navy", line_width=4, legend="Minimo de Llamadas Recibidas")
	unique_id.line(x=meses, y=valoresminA, color="red", line_width=4, legend="Minimo de Llamadas Atendidas")

	totalTiempoReg.append(unique_id)
	gridtotalTiempo = gridplot(totalTiempoReg, ncols=1, plot_width=1600, plot_height=728)
	show(gridtotalTiempo)
	reset_output()
	return HttpResponseRedirect(url)


'''
Comentarios para funciones que pueden ayudar en un futuro
resultados = "assets/img/resultado.html";
proceso = call("rm assets/img/resultado.html", shell=True)

cms = [file for file in cms if '.csv' in file]
encuesta = [file for file in encuesta if '.csv' in file]

rutaCms = "assets/recursos/csv/cms/"
rutaEncuesta = "assets/recursos/csv/encuesta/"

resultadosCms = []
resultadosEncuesta = []

cms = os.listdir(rutaCms)
encuesta = os.listdir(rutaEncuesta)
'''