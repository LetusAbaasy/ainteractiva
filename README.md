# AInteractiva (AI)

## Requisitos

Django > 1.6
Python > 2.7

### Prerequisitos

Qué cosas necesitas para instalar el software y cómo instalarlas:

Actulizar Sistema Operativo

```
# apt-get update
# apt-get dist-upgrade ó upgrade
```

Paquetes de pyhton requeridos 
```
# pip install pandas
# pip install Jinja2>=2.7 numpy>=1.7.1 packaging>=16.8 pillow>=4.0 PyYAML>=3.10 six>=1.5.2 tornado>=4.3
# pip install bokeh
# pip install wordcloud
# pip install nltk
```

Si es necesario instalar Git

```
# apt-get install git
```

## Corriendo el proyecto - Dev

```
# pyhton manager.py runserver
```

Abrimos el explorador y entramos a la ip del servidor al proyecto por ejemplo: http://127.0.0.1:8000/home/

## Creado con

* [Django](https://www.djangoproject.com/) - Framework

## Versiones

Se usa [SemVer](http://semver.org/) para versiones.

## Autores

* **Sergio Daniel Martínez Porras** - *Trabajo Inicial* - [M4RS](https://bitbucket.org/M4RS/)